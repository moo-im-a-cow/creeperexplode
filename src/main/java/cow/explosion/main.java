package cow.explosion;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class main extends JavaPlugin implements Listener {
    List<Material> destroyable = new ArrayList<>();

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        this.saveDefaultConfig();

        this.getConfig().getStringList("creeper").forEach(name -> {
            try{
                destroyable.add(Material.valueOf(name));
                this.getLogger().info("Creepers can destroy: "+ name);
            } catch (IllegalArgumentException ex){
                this.getLogger().info("Material not found: "+ name);
            }
        });


    }
    @EventHandler
    public void creeperexplode(org.bukkit.event.entity.EntityExplodeEvent ev) {
     //   destroyable.add(Material.CHEST);
    //    destroyable.add(Material.TRAPPED_CHEST);
        Entity entity = ev.getEntity();
        if (entity.getType().equals(EntityType.CREEPER))
            ev.blockList().removeIf(block -> !destroyable.contains(block.getType()));
    }
}


